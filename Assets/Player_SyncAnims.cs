﻿using UnityEngine;
using System.Collections;

public class Player_SyncAnims : MonoBehaviour
{

    public Animator thirdPerson = null;
    public bool isRunning       = false;
    public bool isReloading     = false;
    public bool isStrafingLeft  = false;
    public bool isStrafingRight = false;
    public bool SwitchWeapon    = false;
    public bool isWalking       = false;
	
	// Update is called once per frame
	void Update ()
    {
        if(!GetComponent<Player_NetworkManager>().isLocalPlayer)
            SyncAnimations();
	}
    
    public void SyncAnimations()
    {
        thirdPerson.SetBool("isRunning", isRunning);
        thirdPerson.SetBool("isReloading", isReloading);
        thirdPerson.SetBool("isStrafingLeft", isStrafingLeft);
        thirdPerson.SetBool("isStrafingRight", isStrafingRight);
        thirdPerson.SetBool("SwitchWeapon", SwitchWeapon);
        thirdPerson.SetBool("isWalking", isWalking);
    }    
}
