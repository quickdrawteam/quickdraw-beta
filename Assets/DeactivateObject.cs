﻿using UnityEngine;
using System.Collections;

public class DeactivateObject : MonoBehaviour
{

    public GameObject obj;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(obj != null && obj.activeSelf == true)
        {
            obj.SetActive(false);
        }

    }
}
