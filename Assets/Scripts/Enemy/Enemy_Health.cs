﻿using UnityEngine;
using System.Collections;

public class Enemy_Health : MonoBehaviour
{
    [Header("Health")]
    public float health;

    [Header("Drop")]
    public GameObject drop;
    public float dropPercentage;

    private GameObject player;
    private GameObject enemy;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        CheckIfAlive();
        FindLocalPlayer();
    }

    public void TakeDamage(float dmg)
    {
        health -= dmg;
        GetComponent<Enemy_AI>().currentState = Enemy_AI.State.FLEEING;
        GetComponent<Enemy_AI>().ResetFleeTimer();
        if (health <= 0)
            player.GetComponent<Softcore_Objectives>().AddKill();
    }

    void CheckIfAlive()
    {
        if (health <= 0)
        {
            if (RandomChance() < dropPercentage)
            {
                GameObject newDrop = Instantiate(drop) as GameObject;
                newDrop.transform.position = transform.position;
            }
            Destroy(gameObject);
        }
    }

    float RandomChance()
    {
        return Random.Range(0, 100);
    }

    void FindLocalPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length == 2)
        {
            float player1distance = Vector3.Distance(players[0].transform.position, transform.position);
            float player2distance = Vector3.Distance(players[1].transform.position, transform.position);
            if (player1distance < player2distance)
            {
                player = players[0];
                enemy = players[1];
            }
            else
            {
                player = players[1];
                enemy = players[0];
            }
        }
        else if (players.Length == 1)
            player = players[0];
    }
}
