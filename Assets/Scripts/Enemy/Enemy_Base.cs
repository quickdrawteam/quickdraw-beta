﻿using UnityEngine;
using System.Collections;
using System.Linq;


public class Enemy_Base : MonoBehaviour
{
    #region Variables
    [Header("States")]
    public State currentState;               //What state 'this' is in
    public enum State
    {
        SEEKING,
        IDLE,
        WANDER,
        SHOOTING,
        FLEEING,
    }

    [Header("Type")]
    public bool isBoss;
    public bool ranged;
    public bool sweep_clear_flag = false;

    [Header("Health")]
    public float health;                     //How much health 'this' has

    [Header("Shooting")]
    public int bulletDamage;               //How much dmg 'this' does
    public float timeBetweenShots;           //How long in between each bullet
    public float fireSpeed;                  //How fast 'this' can shoot
    public float bulletSpread;               //How wide the bullets go after being shot
    public float bulletRange;                //How far the enemies bulets go 
    public float shootRadius;                //How wide the enemies shoot
    public float tracerSpeed;                //How fast the tracers move
    public float bulletsPerShot;
    private float rayCastCounter;             //How many rayCasts there have been
    private float recentlyShot;               //Weather 'this' has recently shot or not
    private float firstSeenTimer;
    public float firstSeenDelay;             //Adds delay to shooting depending on if just seen player
    public float explosionDamage = 500;
    public float explosionRadius = 500;
    public float headshotMult = 1.5f;

    [Space(20)]
    public GameObject tracerBullet;               //The tracer prefab that is shot from the gun when shot
    public GameObject raycastOrigin;              //Where the rayCast is sent from
    public GameObject bulletOrigin;               //Where the bullet and tracer comes from

    [Header("Movement")]
    public float movementSpeed;              //How fast the enemy moves across the terrain
    public float wanderRadius;               //How far away from 'this' the enemy wanders
    public float stoppingDistanceToTarget;   //How far away 'this' can get before stopping movement
    public float detectionRadius;            //How close 'this' needs to get to closest_Target before detecting them
    private float wanderTimer;                //Adds a delay before getting a new wander closestObject location
    private float fleeTimer;
    private float initFleeTimer;
    private bool fleeing;
    private string barrierTag;
    private NavMeshAgent navMeshAgent;               //The navMeshAgent agent that moves the player around the terrain

    [Header("Drop")]
    public GameObject itemDropped;                //What item is dropped by the enemy when 'this' dies
    public float dropPercentage;

    public GameObject secondItemDropped;                //What item is dropped by the enemy when 'this' dies
    public float secondDropPercentage;

    private float chanceVar = 0;

    public ParticleSystem deathEffect;

    [Header("Misc")]
    [HideInInspector]
    public GameObject closest_target;              //The closest 'player' or 'interacterable' object
    private GameObject player;
    private GameObject enemy;
    [HideInInspector]
    public bool dead;
    private Animator animator;
    private bool active;

    [Header("Audio")]
    public AudioSource gunShootingSound;                //What sound 'this' produces when shooting                 
    public AudioSource deathSound;                //What sound 'this' produces when shooting       
    public AudioSource currentSound;

    public AudioSource[] sounds;
    #endregion

    #region Functions
    void Start()
    {
        active = true;
        navMeshAgent = gameObject.GetComponent<NavMeshAgent>();
        currentState = State.WANDER;
        navMeshAgent.speed = movementSpeed;
        recentlyShot = timeBetweenShots;
        wanderTimer = 3;
        animator = GetComponent<Animator>();
        initFleeTimer = fleeTimer;
        if (sounds.Length > 0)
        {
            currentSound = sounds[Random.Range(0, sounds.Length)];
        }
        bulletOrigin.transform.position = this.transform.position;
        bulletOrigin.transform.position += bulletOrigin.transform.forward.normalized * 0.5f;
        bulletOrigin.transform.position += new Vector3(0, navMeshAgent.height / 2, 0);
    }

    void FixedUpdate()
    {
        if (active)
        {
            FindClosestObject();

            if (isBoss)
            {
                animator.SetBool("isRunning", true);
            }
        }
    }

    void Update()
    {
        if (active)
        {
            CheckForChangeState();
            UpdateState();

            CheckIfAlive();
            //FindLocalPlayer();
            animator.SetFloat("moveSpeed", (GetComponent<NavMeshAgent>().velocity / movementSpeed).normalized.magnitude);
        }
    }

    void UpdateState()
    {
        switch (currentState)
        {
            case State.SEEKING:
                SeekPlayer();
                break;
            case State.IDLE:
                Idle();
                break;
            case State.WANDER:
                Wander();
                break;
            case State.SHOOTING:
                Shooting();
                break;
            case State.FLEEING:
                Flee();
                break;
            default:
                break;
        }
    }

    void CheckForChangeState()
    {
        if (fleeing) return;

        #region Seeking & Shooting
        //If can see player, seekforplayer
        FindClosestObject();

        if (closest_target == null) return;

        if (Vector3.Distance(transform.position, closest_target.transform.position) < detectionRadius)
        {
            //Raycast to see if you can see player
            RaycastHit hitInfo;
            if (Physics.Raycast(transform.position, (closest_target.transform.position - transform.position), out hitInfo))
            {
                if (hitInfo.transform.gameObject == closest_target)
                {
                    firstSeenTimer += Time.deltaTime;
                    if (firstSeenTimer >= firstSeenDelay)
                    {
                        recentlyShot += Time.deltaTime;
                        if (recentlyShot >= timeBetweenShots)
                        {
                            ChangeState(State.SHOOTING);
                            recentlyShot = 0;
                            Invoke("ChangeStateSeeking", 0.5f);
                        }
                    }
                }
                else
                {
                    firstSeenTimer = 0;
                    ChangeState(State.WANDER);
                }
            }
        }
        #endregion
    }

    void ChangeStateSeeking()
    {
        currentState = State.SEEKING;
    }

    void SeekPlayer()
    {
        if (closest_target == null) return;

        if (Vector3.Distance(transform.position, closest_target.transform.position) < stoppingDistanceToTarget)
        {
            navMeshAgent.Stop();
            //Die!!!
            animator.SetBool("isRunning", false);
        }
        else
        {
            navMeshAgent.SetDestination(closest_target.transform.position);
            navMeshAgent.Resume();
            animator.SetBool("isRunning", true);
            //Play "Where is he" sound
        }
    }

    void Idle()
    {
        navMeshAgent.Stop();
        //Start idle animation
    }

    void Flee()
    {
        if (fleeTimer > 0)
        {
            fleeing = true;
            fleeTimer -= Time.deltaTime;

            if (isBoss)
            {
                if (navMeshAgent.SetDestination(SearchForCover(null).position))
                {
                    //Play flee sound

                    //navMeshAgent.SetDestination(SearchForCover(null).position);
                    if (Vector3.Distance(transform.position, SearchForCover(null).position) < 0.1f)
                    {
                        Vector3 direction = Camera.current.transform.position - raycastOrigin.transform.position;

                        Ray r = new Ray(raycastOrigin.transform.position, direction);
                        RaycastHit hit;

                        if (Physics.Raycast(r, out hit))
                        {
                            Debug.DrawRay(transform.position, direction);

                            if (hit.transform.gameObject.tag == "Body" || hit.transform.gameObject.tag == "Head" || hit.transform.gameObject.tag == "Player")
                            {
                                if (closest_target == null) return;

                                SearchForCover(closest_target.transform);
                            }
                        }
                    }
                }
            }
            else
            {
                navMeshAgent.SetDestination(RandomNavSphere(transform.position, wanderRadius, 0));
            }
        }
        else
        {
            fleeing = false;
            fleeTimer = initFleeTimer;
            ChangeState(State.WANDER);
        }
    }

    void Wander()
    {
        wanderTimer += Time.deltaTime;

        if (wanderTimer >= 3)
        {
            Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
            navMeshAgent.SetDestination(newPos);
            wanderTimer = 0;
            //Play wandering sound
        }
    }

    void Shooting()
    {
        if (ranged)
        {
            navMeshAgent.SetDestination(transform.position);
        }
        if (health >= 0)
        {
            if (rayCastCounter <= 0)
            {
                //checks to see if the enemy can see the player
                rayCastCounter = fireSpeed;

                if (closest_target == null) return;

                Vector3 direction = closest_target.transform.position - raycastOrigin.transform.position;

                direction += new Vector3(0, 1, 0);
                Ray r = new Ray(raycastOrigin.transform.position, direction);
                RaycastHit hit;

                if (Physics.Raycast(r, out hit))
                {
                    Debug.DrawRay(transform.position, direction);

                    //if the player is seen
                    if (hit.collider.gameObject.tag == "Body" || hit.collider.tag == "Head" || hit.transform.gameObject.tag == "Player" || hit.transform.gameObject.tag == "interactableObjective")
                    {
                        ShootBurst();
                    }
                    else
                    {
                        firstSeenTimer -= Time.deltaTime;
                        if (firstSeenTimer <= 0)
                        {
                            firstSeenTimer = 0;
                        }
                    }
                }
                else
                {
                    //move to new location
                    for (int i = 0; i < 30; ++i)
                        Debug.DrawLine(raycastOrigin.transform.position, hit.point);
                }
            }
            //stops the enemy raycasting every frame to help performance
            if (rayCastCounter > 0)
                rayCastCounter -= Time.deltaTime;
        }
    }

    void FindClosestObject()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        GameObject[] gameObjectslist = GameObject.FindGameObjectsWithTag("interactableObjective");

        float minDist = Mathf.Infinity;

        foreach (GameObject t in players)
        {
            if (t.tag == "interactableObjective")
            {
                if (closest_target.GetComponent<Objective_Generator>().active == false)
                {
                    return;
                }
            }
            float dist = Vector3.Distance(t.transform.position, transform.position);
            if (dist < minDist)
            {
                closest_target = t;
                minDist = dist;
            }
        }

        foreach (GameObject t in gameObjectslist)
        {
            if (t.tag == "interactableObjective")
            {
                if (closest_target.GetComponent<Objective_Generator>().active == false)
                {
                    return;
                }
            }
            float dist = Vector3.Distance(t.transform.position, transform.position);
            if (dist < minDist)
            {
                closest_target = t;
                minDist = dist;
            }
        }


    }

    void ShootRay()
    {
        if (currentSound != null)
        {
            if (!currentSound.isPlaying)
            {
                currentSound.Play();
                Invoke("AssignNewSound", currentSound.clip.length);
            }
        }
        else
        {
            if (gunShootingSound != null)
                PlayAttackSound();
        }

        if (closest_target == null) return;

        float randomRadius = Random.Range(0, bulletSpread);                                     // ray casts will be in a circle area
        float randomAngle = Random.Range(0, 2 * Mathf.PI);                                      // ray casts will be in a circle area
        transform.LookAt(closest_target.gameObject.transform.position);                      //look at the player before shooting forwards
        Vector3 direction = new Vector3(randomRadius * Mathf.Cos(randomAngle),
                            randomRadius * Mathf.Sin(randomAngle), bulletRange);                //Calculate raycast direction

        direction = bulletOrigin.transform.TransformDirection(direction.normalized);            //make direction match the transform


        // eg. converting the vector3 forward to transform forward

        Ray r = new Ray(raycastOrigin.transform.position, direction);                           //raycast to the player
        RaycastHit hit;

        if ((transform.position - closest_target.transform.position).magnitude < shootRadius)
        {
            if (!isBoss)
            {
                GetComponent<Animator>().SetTrigger("attack");
            }
            if (Physics.Raycast(r, out hit))
            {
                if (ranged)
                {
                    ShootTracer(direction);
                }
                switch (hit.collider.tag)
                {
                    case "Player":
                        if (hit.collider.transform.root.GetComponent<Player_Health>())
                        {
                            hit.collider.transform.root.GetComponent<Player_Health>().TakeDamage(bulletDamage);
                        }
                        break;
                    case "Body":
                        if (hit.collider.transform.root.GetComponent<Player_Health>())
                        {
                            hit.collider.transform.root.GetComponent<Player_Health>().TakeDamage(bulletDamage);
                        }
                        break;
                    case "Head":
                        if (hit.collider.transform.root.GetComponent<Player_Health>())
                        {
                            hit.collider.transform.root.GetComponent<Player_Health>().TakeDamage(bulletDamage);
                        }
                        break;
                    case "interactableObjective":
                        hit.collider.gameObject.GetComponent<Objective_Generator>().TakeDamage(bulletDamage);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    void AssignNewSound()
    {
        currentSound = sounds[Random.Range(0, sounds.Length)];
    }

    void ShootBurst()
    {
        for (int i = 0; i < bulletsPerShot; i++)
        {
            Invoke("ShootRay", ((i + 1) / 10) - 0.1f);
        }
    }

    void ChangeState(State nextState)
    {
        currentState = nextState;
    }

    void ShootTracer(Vector3 direction)
    {
        Vector3 origin = bulletOrigin.transform.position;
        GameObject tracer = Instantiate(tracerBullet, origin + direction, transform.rotation) as GameObject;
        tracer.GetComponent<Rigidbody>().AddForce(direction * tracerSpeed);
        Destroy(tracer, 1);
    }

    Transform SearchForCover(Transform notInclude)
    {
        GameObject[] closestObjects;
        closestObjects = GameObject.FindGameObjectsWithTag(barrierTag.ToString());
        closest_target = closestObjects[0];
        for (int i = 1; i < closestObjects.Length; i++)
        {
            float first = Vector3.Distance(closestObjects[i].transform.position, transform.position);
            float second = Vector3.Distance(closestObjects[i - 1].gameObject.transform.position, transform.position);
            if (first < second)
            {
                closest_target = closestObjects[i];
            }
        }
        return closest_target.transform;
    }

    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layermask)
    {
        Vector3 randDirection = Random.insideUnitSphere * dist;

        randDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randDirection, out navHit, dist, layermask);

        return navHit.position;
    }

    public void ResetFleeTimer()
    {
        fleeTimer = initFleeTimer;
    }

    //Health
    void CheckIfAlive()
    {
        if (health <= 0)
        {
            if (sweep_clear_flag)
            {
                transform.parent.GetComponent<Sweep_Clear>().TakeEnemyOutOfList(this.gameObject);
            }
            RandomChance();
            if (chanceVar < dropPercentage)
            {
                GameObject newDrop = Instantiate(itemDropped) as GameObject;
                newDrop.transform.position = transform.position;

                //Check if it came from sweep and clear objective, if so, and it dies, take it out of the list
            }
            else if (chanceVar < dropPercentage + secondDropPercentage)
            {
                GameObject newDrop = Instantiate(secondItemDropped) as GameObject;
                newDrop.transform.position = transform.position;
            }
            Death();
            Destroy(gameObject, 3.5f);
            active = false;
        }
    }

    void FindLocalPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length == 2)
        {
            float player1distance = Vector3.Distance(players[0].transform.position, transform.position);
            float player2distance = Vector3.Distance(players[1].transform.position, transform.position);
            if (player1distance < player2distance)
            {
                player = players[0];
                enemy = players[1];
            }
            else
            {
                player = players[1];
                enemy = players[0];
            }
        }
        else if (players.Length == 1)
            player = players[0];
    }

    void RandomChance()
    {
        chanceVar = Random.Range(0, 100);
    }

    public void TakeDamage(float dmg)
    {
        health -= dmg;
        currentState = State.FLEEING;
        ResetFleeTimer();
        if (health <= 0)
        {
            dead = true;
        }
    }

    public void Headshot(float dmg)
    {
        health -= dmg * headshotMult;
        currentState = State.FLEEING;
        ResetFleeTimer();
        if (health <= 0)
        {
            if (player.GetComponent<Player_Buffs>().explosiveHeadshot)
            {
                Collider[] cols = Physics.OverlapSphere(transform.position, explosionRadius);
                foreach (Collider c in cols)
                {
                    if (c.tag == "Enemy")
                    {
                        float damage = explosionDamage - Vector3.Distance(transform.position, c.transform.position);

                        c.GetComponent<Enemy_Base>().TakeDamage(damage);
                    }
                }
            }
        }
    }

    void PlayAttackSound()
    {
        if (!gunShootingSound.isPlaying)
        {
            gunShootingSound.Play();
        }
    }

    void Death()
    {
        transform.FindChild("ProperModel").gameObject.SetActive(false);
        GetComponent<Animator>().enabled = false;
        GetComponent<CapsuleCollider>().enabled = false;
        GetComponent<NavMeshAgent>().enabled = false;
        transform.FindChild("Ragdoll").gameObject.SetActive(true);
        //SpawnDeath Effects
        transform.FindChild("Ragdoll").GetComponent<Rigidbody>().AddForce(-transform.forward * 10);
        // Invoke("SpawnDeathParticles", 2.9f);
        if (deathSound != null)
            deathSound.Play();
    }

    void SpawnDeathParticles()
    {
        if (deathEffect != null)
        {
            ParticleSystem newThing = Instantiate(deathEffect, transform.position, transform.rotation, transform) as ParticleSystem;
            Destroy(newThing, 1);
        }
    }
    #endregion
}
