﻿using UnityEngine;
using System.Collections;

public class Enemy_Spawn_Trigger : MonoBehaviour
{
    public GameObject spawner;


    void Start()
    {
        if (spawner == null)
            spawner = transform.parent.gameObject;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Body")
        {
            spawner.GetComponent<Enemy_Spawn>().spawnEnemies = true;
        }
    }
}
