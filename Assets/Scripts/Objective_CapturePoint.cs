﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Objective_CapturePoint : MonoBehaviour
{
    #region Variables
    #region Buffs  
    public float speedProbability   = 0;
    public float healthProbability  = 0;
    public float damageProbability  = 0;
    public float headshotProb       = 100;
    public List<BuffProb> probList;
    public class BuffProb
    {
        public float probability;
        public float cumulativeProb;
        public float buffValue;
        public int buffID;
    }
    #endregion
    #region Others
    public float        buffValue;

    public float        captureDistance     = 5.0f;
    public float        captureTimeTotal    = 3.0f;
    private float       captureProgress     = 0.0f;

    public GameObject   player              = null;
    public GameObject   enemyPlayer         = null;
    private Image       progressBar         = null;
    private Canvas      canvas              = null;

    public int          buffID;
    public bool         buffSelected        = false;
    private bool        captured            = false;
    private bool        updateProgressBar   = false;
    private bool        active              = false;
    #endregion  
    #endregion
    
    void Start()
    {
        #region Buff Settings And Startup
        probList                = new List<BuffProb>();    //Instantiates new list

        BuffProb speed          = new BuffProb();   //Making new speed value
        BuffProb health         = new BuffProb();   //Making new health value
        BuffProb damage         = new BuffProb();   //Making new health value
        BuffProb headshot       = new BuffProb();

        // Movement
        speed.cumulativeProb    = 0;
        speed.probability       = speedProbability;
        speed.buffID            = 1;

        // Damage
        damage.cumulativeProb   = 0;
        damage.probability      = damageProbability;
        damage.buffID           = 3;

        // Health
        health.cumulativeProb   = 0;
        health.probability      = healthProbability;
        health.buffID           = 4;

        // Headshot
        headshot.cumulativeProb = 0;
        headshot.probability    = headshotProb;
        headshot.buffID         = 9;

        probList.Add(health);
        probList.Add(speed);
        probList.Add(damage);
        probList.Add(headshot);
        #endregion
        RandomBuff();
    }

    void Setup()
    {
        if (player == null)
            FindLocalPlayer();

        FindLocalPlayer();

        if (canvas == null && player != null)
            canvas = player.GetComponent<Player_Health>().canvasInstance;

        if (canvas != null && progressBar == null)
        {
            progressBar = canvas.transform.FindChild("CaptureProgress").GetComponent<Image>();
            progressBar.material.SetFloat("_Progress", 0);

        }
    }

    // Update is called once per frame
    void Update()
    {
        Setup();
        CheckDistance();
        CheckCompletion();
    }

    void CheckCompletion()
    {
        if (captured && !buffSelected)
        {
            FindLocalPlayer();
            player.GetComponent<Player_Buff_Manager>().AddBuffToList(buffID);
            buffSelected = true;
            DeSpawn();
        }
    }

    void CheckDistance()
    {
        if (player != null && canvas != null && !captured)
        {

            if ((player.transform.position - transform.position).magnitude <= captureDistance)
            {
                captureProgress += (Time.deltaTime);
                active = true;
            }
            else if ((player.transform.position - transform.position).magnitude > captureDistance && captureProgress >= 0)
            {
                captureProgress -= (Time.deltaTime) / 2;
                active = false;
            }

            if (active)
            {
                if (player.GetComponent<SetupLocalPlayer>().CheckIfLocalPlayer())
                {
                    float progressBarValue = ((captureProgress / captureTimeTotal));
                    progressBar.material.SetFloat("_Progress", progressBarValue);
                }
            }
        }

        if (captureProgress >= captureTimeTotal)
            captured = true;
    }

    void FindLocalPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length == 2)
        {
            float player1distance = Vector3.Distance(players[0].transform.position, transform.position);
            float player2distance = Vector3.Distance(players[1].transform.position, transform.position);
            if (player1distance < player2distance)
            {
                player = players[0];
                enemyPlayer = players[1];
            }
            else
            {
                player = players[1];
                enemyPlayer = players[0];
            }
        }
        else if (players.Length == 1)
            player = players[0];


    }
    
    void RandomBuff()
    {
        probList[0].cumulativeProb = probList[0].probability;
        int randomNumber = Random.Range(0, 100);

        if (probList[0].cumulativeProb > randomNumber)
        {
            buffID = probList[0].buffID;
            buffValue = probList[0].buffValue;
            return;
        }

        for (int i = 1; i < probList.Count; ++i)
        {
            probList[i].cumulativeProb = probList[i].probability + probList[i - 1].cumulativeProb;

            if (probList[i].cumulativeProb > randomNumber)
            {
                buffID = probList[i].buffID;
                buffValue = probList[i].buffValue;
                return;
            }
        }
    }
    
    void DeSpawn()
    {
        captureProgress = 0;
        progressBar.material.SetFloat("_Progress", 0);
        Destroy(gameObject, 1);
        //transform.GetChild(0).GetComponent<IndicatorScript>().DestroyIndicator(1);
    }
}
