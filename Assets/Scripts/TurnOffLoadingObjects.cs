﻿using UnityEngine;
using System.Collections;

public class TurnOffLoadingObjects : MonoBehaviour
{

    public GameObject loadingRings = null;
    public GameObject loadingPanel = null;

	// Use this for initialization
	void Start ()
    {
    }

    void Update()
    {
        if (loadingRings != null)
            loadingRings.SetActive(false);

        if (loadingPanel != null)
            loadingPanel.SetActive(false);
    }
}
