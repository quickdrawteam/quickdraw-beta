﻿using UnityEngine;
using System.Collections;

public class Screen_Shake : MonoBehaviour
{
    private Vector3 originPosition;

    private Quaternion originRotation;

    [HideInInspector]
    public float shake_decay;

    public float shake_decay_val = 0.002f;

    [HideInInspector]
    public float shake_intensity;

    public float shake_intensity_val = 0.03f;

    private bool shaking;

    private Transform _transform;


    void OnEnable()
    {

        _transform = transform;

    }

    void Update()
    {

        if (!shaking)
            return;

        if (shake_intensity > 0f)
        {

            _transform.localPosition = originPosition + Random.insideUnitSphere * shake_intensity * Time.deltaTime;

            _transform.localRotation = new Quaternion(
            originRotation.x + Random.Range(-shake_intensity, shake_intensity) * .2f,
            originRotation.y + Random.Range(-shake_intensity, shake_intensity) * .2f,
            originRotation.z + Random.Range(-shake_intensity, shake_intensity) * .2f,
            originRotation.w + Random.Range(-shake_intensity, shake_intensity) * .2f);

            shake_intensity -= shake_decay;

        }
        else
        {
            shaking = false;
            _transform.localPosition = originPosition;
            _transform.localRotation = originRotation;
        }

    }

    public void ShakeCamera()
    {

        if (!shaking)
        {
            originPosition = _transform.localPosition;
            originRotation = _transform.localRotation;
        }

        shaking = true;
        shake_intensity = shake_intensity_val;
        shake_decay = shake_decay_val;
    }

    public void ShakeCamera(float intensity, float decay)
    {

        if (!shaking)
        {
            originPosition = _transform.localPosition;
            originRotation = _transform.localRotation;
        }

        shaking = true;
        shake_intensity = intensity;
        shake_decay = decay;
    }

}
