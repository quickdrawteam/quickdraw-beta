﻿using UnityEngine;
using System.Collections;

public class Flag_Script : Base_Interact
{
    private GameObject player;
    private GameObject enemyPlayer;

    // Use this for initialization
    void Start ()
    {

	}
	
	// Update is called once per frame
	void Update ()
    {
	    
	}

    public override void Interact()
    {
        FindLocalPlayer();
        player.GetComponent<Player_Flag>().PickedUpFlag(this.gameObject);
        Destroy(this.gameObject);
    }

    void FindLocalPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length == 2)
        {
            float player1distance = Vector3.Distance(players[0].transform.position, transform.position);
            float player2distance = Vector3.Distance(players[1].transform.position, transform.position);
            if (player1distance < player2distance)
            {
                player = players[0];
                enemyPlayer = players[1];
            }
            else
            {
                player = players[1];
                enemyPlayer = players[0];
            }
        }
        else if (players.Length == 1)
            player = players[0];


    }

}
