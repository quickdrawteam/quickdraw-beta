﻿using UnityEngine;
using System.Collections.Generic;

public class Player_Flag : MonoBehaviour
{
    public bool hasFlag;
    private GameObject flagIcon = null;
    #region Buffs  
    public float speedProbability = 50;
    public float healthProbability = 50;
    public float damageProbability = 0;
    public List<BuffProb> probList;
    private int buffID;
    private float buffValue;

    public class BuffProb
    {
        public float probability;
        public float cumulativeProb;
        public float buffValue;
        public int buffID;
    }
    #endregion
    private GameObject player;
    private GameObject enemyPlayer;

    private bool buffQueued;

    void Start()
    {
        #region Buff Settings And Startup
        probList = new List<BuffProb>();    //Instantiates new list

        BuffProb speed = new BuffProb();   //Making new speed value
        BuffProb health = new BuffProb();   //Making new health value
        BuffProb damage = new BuffProb();   //Making new health value

        // Movement
        speed.cumulativeProb = 0;
        speed.probability = speedProbability;
        speed.buffID = 1;

        // Damage
        damage.cumulativeProb = 0;
        damage.probability = damageProbability;
        damage.buffID = 3;

        // Health
        health.cumulativeProb = 0;
        health.probability = healthProbability;
        health.buffID = 4;


        probList.Add(health);
        probList.Add(speed);
        probList.Add(damage);
        #endregion
        RandomBuff();
        buffQueued = false;
    }

    void Update()
    {
        FindLocalPlayer();

        if (flagIcon == null)
        {
            var canvas = player.GetComponent<Player_Health>().canvasInstance;
            if(canvas != null)
                flagIcon = canvas.transform.FindChild("FlagImage").gameObject;
        }
        else
        {
            if (hasFlag)
            {
                flagIcon.SetActive(true);
            }
            else
                flagIcon.SetActive(false);
        }
    }

    void RandomBuff()
    {
        probList[0].cumulativeProb = probList[0].probability;
        int randomNumber = Random.Range(0, 100);

        if (probList[0].cumulativeProb > randomNumber)
        {
            buffID = probList[0].buffID;
            buffValue = probList[0].buffValue;
            return;
        }

        for (int i = 1; i < probList.Count; ++i)
        {
            probList[i].cumulativeProb = probList[i].probability + probList[i - 1].cumulativeProb;

            if (probList[i].cumulativeProb > randomNumber)
            {
                buffID = probList[i].buffID;
                buffValue = probList[i].buffValue;
                return;
            }
        }
    }

    public void PickedUpFlag(GameObject a_flag)
    {
        //GameObject newFlag = Instantiate(a_flag, GetComponent<Player_ShootManager>().weaponPosition.transform.position, a_flag.transform.localRotation, GetComponent<Player_ShootManager>().weaponPosition.transform) as GameObject;
        hasFlag = true;
    }

    void FindLocalPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length == 2)
        {
            float player1distance = Vector3.Distance(players[0].transform.position, transform.position);
            float player2distance = Vector3.Distance(players[1].transform.position, transform.position);
            if (player1distance < player2distance)
            {
                player = players[0];
                enemyPlayer = players[1];
            }
            else
            {
                player = players[1];
                enemyPlayer = players[0];
            }
        }
        else if (players.Length == 1)
            player = players[0];


    }

    void OnTriggerEnter(Collider other)
    {
        if (hasFlag)
        {
            if (!buffQueued)
            {
                if (other.gameObject.name == "FlagEndPoint")
                {
                    {
                        FindLocalPlayer();
                        player.GetComponent<Player_Buff_Manager>().AddBuffToList(buffID);
                        buffQueued = true;
                        hasFlag = false;
                    }
                }
                else
                {
                }
            }
        }
    }

    public void FlagReturned()
    {
        if (hasFlag)
        {
            if (!buffQueued)
            {
                FindLocalPlayer();
                player.GetComponent<Player_Buff_Manager>().AddBuffToList(buffID);
                buffQueued = true;
                hasFlag = false;
            }
            else
            {
            }

        }
    }
}

