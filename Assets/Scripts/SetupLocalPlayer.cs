﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SetupLocalPlayer : NetworkBehaviour
{
    [SyncVar]
    public string playerName;

    public GameObject thirdPerson = null;
    public GameObject firstPerson = null;

    [SerializeField]
    Behaviour[] componentsToDisable;
    Behaviour[] objectsToDisable;
    //to send message from server to all clients, use [syncvar]
    //to send message form client to server, use [command]
    // Use this for initialization

    void Awake()
    {
        if (isLocalPlayer)
        {
            GetComponent<Player_Health>()._isLocalPlayer = true;
        }
    }

    void Start()
    {
        //if (isLocalPlayer)
        //{
        //    canvas = Instantiate(canvas) as Canvas;
        //    canvas.enabled = true;
        //    canvas.gameObject.SetActive(true);
        //    GetComponent<Player_Movement>().enabled = true;
        //    GetComponent<Player_Shoot>().enabled = true;
        //    transform.GetChild(0).GetComponent<Camera>().enabled = true;
        //    this.GetComponentInChildren<TextMesh>().text = playerName;
        //}
        if (!isLocalPlayer)
        {
            for (int i = 0; i < componentsToDisable.Length; ++i)
            {
                componentsToDisable[i].enabled = false;
            }

            firstPerson.SetActive(false);
        }
        else
        {
            thirdPerson.SetActive(false);
        }
    }

    void Update()
    {
        // this.GetComponentInChildren<TextMesh>().text = playerName;
    }


    public bool CheckIfLocalPlayer()
    {
        return isLocalPlayer;
    }

    public void DisableAllComponents()
    {
        if (isLocalPlayer)
        {
            for (int i = 0; i < componentsToDisable.Length; ++i)
            {
                componentsToDisable[i].enabled = false;
            }
        }
    }
}
