﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
public class Softcore_Objectives : MonoBehaviour
{

    #region Buffs
    public class BuffProb
    {
        public float probability;
        public float cumulativeProb;
        public float buffValue;
        public int buffID;
    }

    public List<BuffProb> probList;
    public float speedProbability = 50;
    public float healthProbability = 50;
    public float damageProbability = 0;
    private bool killsCompleted;
    private bool killsWithoutTakingDamageCompleted;
    private bool headShotsCompleted;

    private bool buffSelected = false;
    public int activeObjectives;

    private List<bool> randomBuffList;
    #endregion
    private Canvas passedInCanvas;
    public GameObject textBoxPrefab;
    public float offset;
    private int numberOfTextBoxesMade;

    class Objective
    {
        public float currentValue;
        public float completionValue;

        public bool instantiated;
        public bool completed;

        public Text text;

        public string name;

        public bool selected = false;
    }

    [Header("Settings")]
    public int killsNeeded;
    public int headShotsNeeded;
    public int killsWithoutTakingDamageNeeded;
    public int speedKillsNeeded;
    public float speedKillTimeLimit;
    public int speedKillAttemptsTotal;

    private int buffID;
    private float buffValue;

    private Objective killsObjective;
    private Objective killsWithoutDmgObjective;
    private Objective headShotObjective;
    private Objective speedKillsObjective;

    void Start()
    {
        passedInCanvas = GetComponent<Player_Health>().canvasInstance;

        killsObjective = new Objective();
        killsWithoutDmgObjective = new Objective();
        headShotObjective = new Objective();
        speedKillsObjective = new Objective();

        #region Init Objective Settings
        killsObjective.completionValue              = killsNeeded;
        killsWithoutDmgObjective.completionValue    = killsWithoutTakingDamageNeeded;
        headShotObjective.completionValue           = headShotsNeeded;
        speedKillsObjective.completionValue         = speedKillsNeeded;
        #endregion  
        #region Buff Settings
        probList = new List<BuffProb>();    //Instantiates new list

        BuffProb speed = new BuffProb();   //Making new speed value
        BuffProb health = new BuffProb();   //Making new health value
        BuffProb damage = new BuffProb();   //Making new damage value

        // Movement
        speed.cumulativeProb = 0;
        speed.probability = speedProbability;
        speed.buffID = 1;

        // Health
        health.cumulativeProb = 0;
        health.probability = healthProbability;
        health.buffID = 4;

        // Damage
        damage.cumulativeProb = 0;
        damage.probability = damageProbability;
        damage.buffID = 3;

        probList.Add(health);
        probList.Add(speed);
        probList.Add(damage);
        #endregion
        #region RandomList
        randomBuffList = new List<bool>();
        randomBuffList.Add(killsObjective.selected);
        randomBuffList.Add(killsWithoutDmgObjective.selected);
        randomBuffList.Add(headShotObjective.selected);
        randomBuffList.Add(speedKillsObjective.selected);
        #endregion
        #region Custom While Loop (Kades a tard)
        for (int i = 0; i < activeObjectives; i++)
        {
            int temp = Random.Range(0, randomBuffList.Count);
            if (randomBuffList[temp] == false)
            {
                randomBuffList[temp] = true;
            }
            else
            {
                --i;
            }
        }
        #endregion
        RandomBuff();
    }

    void Update()
    {
        UpdateUI();
    }

    void UpdateUI()
    {
        if (passedInCanvas != null)
        {
            #region Kills Total
            if (randomBuffList[0])
            {
                #region Instantiate
                if (!killsObjective.instantiated)
                {
                    GameObject newTextThingy = Instantiate(textBoxPrefab, textBoxPrefab.transform.position, textBoxPrefab.transform.rotation) as GameObject;
                    newTextThingy.transform.position += new Vector3(0, numberOfTextBoxesMade * offset, 0);
                    newTextThingy.transform.parent = passedInCanvas.transform.FindChild("SoftcoreObjectives").transform;

                    numberOfTextBoxesMade++;
                    newTextThingy.name = "Kills";


                    killsObjective.text = newTextThingy.GetComponent<Text>();
                    killsObjective.instantiated = true;
                }
                #endregion
                #region Changing Text
                killsObjective.text.text = "ENEMY KILLS: " + killsObjective.currentValue.ToString() + "/" + killsNeeded.ToString();
                killsObjective.text.color = new Color(1, 1, 1, 0.3f);
                #endregion
                #region Check Completion
                if (killsObjective.currentValue >= killsObjective.completionValue && !killsObjective.completed)
                {
                    //Begin Animation
                    passedInCanvas.transform.FindChild("SoftcoreObjectives").transform.FindChild("Kills").GetComponent<Animator>().SetBool("Completed", true);
                    Destroy(killsWithoutDmgObjective.text.gameObject, 1);
                    RandomBuff();
                    randomBuffList[0] = false;
                    killsObjective.completed = true;
                    GetComponent<Player_Buff_Manager>().AddBuffToList(buffID);
                    buffSelected = false;
                }
                #endregion
            }

            #endregion
            #region Kills Without Taking Damage
            if (randomBuffList[1])
            {
                #region Instantiate
                if (!killsWithoutDmgObjective.instantiated)
                {
                    GameObject newTextThingy = Instantiate(textBoxPrefab, textBoxPrefab.transform.position, textBoxPrefab.transform.rotation) as GameObject;
                    newTextThingy.transform.position += new Vector3(0, numberOfTextBoxesMade * offset, 0);
                    newTextThingy.transform.parent = passedInCanvas.transform.FindChild("SoftcoreObjectives").transform;

                    newTextThingy.name = "KillsWithoutTakingDamage";
                    numberOfTextBoxesMade++;

                    killsWithoutDmgObjective.text = newTextThingy.GetComponent<Text>();
                    killsWithoutDmgObjective.instantiated = true;
                }
                #endregion
                #region Changing Text
                if (killsWithoutDmgObjective != null && !killsWithoutTakingDamageCompleted)
                {
                    if (killsWithoutDmgObjective != null && killsWithoutDmgObjective.text != null)
                    {
                        killsWithoutDmgObjective.text.text = "ENEMY KILLS WITHOUT TAKING DAMAGE: " + killsWithoutDmgObjective.currentValue.ToString() + "/" + killsWithoutDmgObjective.completionValue.ToString();
                        killsWithoutDmgObjective.text.color = new Color(1, 1, 1, 0.3f);
                    }
                }
                #endregion
                #region Check Completion
                if (killsWithoutDmgObjective.currentValue >= killsWithoutDmgObjective.completionValue && !killsWithoutDmgObjective.completed && killsWithoutDmgObjective != null)
                {
                    //Begin Animation
                    killsWithoutDmgObjective.completed = true;
                    if(passedInCanvas.transform.FindChild("SoftcoreObjectives").transform.FindChild("KillsWithoutTakingDamage") != null)
                        passedInCanvas.transform.FindChild("SoftcoreObjectives").transform.FindChild("KillsWithoutTakingDamage").GetComponent<Animator>().SetBool("Completed", true);
                    Destroy(killsWithoutDmgObjective.text.gameObject, 1);
                    RandomBuff();
                    randomBuffList[1] = false;
                    GetComponent<Player_Buff_Manager>().AddBuffToList(buffID);
                    buffSelected = false;
                }
                #endregion
            }
            #endregion
            #region Headshots
            if (randomBuffList[2])
            {
                #region Instantiate
                if (!headShotObjective.instantiated)
                {
                    GameObject newTextThingy = Instantiate(textBoxPrefab, textBoxPrefab.transform.position, textBoxPrefab.transform.rotation) as GameObject;
                    newTextThingy.transform.position += new Vector3(0, numberOfTextBoxesMade * offset, 0);
                    newTextThingy.transform.parent = passedInCanvas.transform.FindChild("SoftcoreObjectives").transform;

                    newTextThingy.name = "HeadShots";
                    numberOfTextBoxesMade++;


                    headShotObjective.text = newTextThingy.GetComponent<Text>();
                    headShotObjective.instantiated = true;
                }
                #endregion
                #region Changing Text
                headShotObjective.text.text = "ENEMY HEADSHOTS: " + headShotObjective.currentValue.ToString() + "/" + headShotObjective.completionValue.ToString();
                headShotObjective.text.color = new Color(1, 1, 1, 0.3f);
                #endregion
                #region Check Completion
                if (headShotObjective.currentValue >= headShotObjective.completionValue && !headShotObjective.completed)
                {
                    //Begin Animation
                    passedInCanvas.transform.FindChild("SoftcoreObjectives").transform.FindChild("HeadShots").GetComponent<Animator>().SetBool("Completed", true);
                    Destroy(headShotObjective.text.gameObject, 1);
                    randomBuffList[2] = false;
                    RandomBuff();
                    headShotObjective.completed = true;
                    GetComponent<Player_Buff_Manager>().AddBuffToList(buffID);
                    buffSelected = false;
                }
                #endregion
            }
            #endregion
            #region Speed Kills
            if (randomBuffList[3])
            {
                #region Instantiate
                if (!speedKillsObjective.instantiated)
                {
                    GameObject newTextThingy = Instantiate(textBoxPrefab, textBoxPrefab.transform.position, textBoxPrefab.transform.rotation) as GameObject;
                    newTextThingy.transform.position += new Vector3(0, numberOfTextBoxesMade * offset, 0);
                    newTextThingy.transform.parent = passedInCanvas.transform.FindChild("SoftcoreObjectives").transform;

                    newTextThingy.name = "SpeedKills";
                    numberOfTextBoxesMade++;

                    speedKillsObjective.text = newTextThingy.GetComponent<Text>();
                    speedKillsObjective.instantiated = true;
                }
                #endregion
                #region Changing Text
                speedKillsObjective.text.text = "SPEED KILLS: " + speedKillsObjective.currentValue.ToString() + "/" + speedKillsObjective.completionValue.ToString();
                speedKillsObjective.text.color = new Color(1, 1, 1, 0.3f);
                #endregion
                #region Check Completion
                if (speedKillsObjective.currentValue >= speedKillsObjective.completionValue && !speedKillsObjective.completed)
                {
                    //Begin Animation
                    passedInCanvas.transform.FindChild("SoftcoreObjectives").transform.FindChild("SpeedKills").GetComponent<Animator>().SetBool("Completed", true);
                    Destroy(speedKillsObjective.text.gameObject, 1);
                    randomBuffList[3] = false;
                    RandomBuff();
                    speedKillsObjective.completed = true;
                    GetComponent<Player_Buff_Manager>().AddBuffToList(buffID);
                    buffSelected = false;
                }
                #endregion
            }
            #endregion
        }
    }

    public void AddKill()
    {
        killsObjective.currentValue++;
        killsWithoutDmgObjective.currentValue++;
    }

    public void AddHeadShot()
    {
        headShotObjective.currentValue++;
    }

    public void AddSpeedKill()
    {
        speedKillsObjective.currentValue++;
    }

    public void ResetSpeedKills()
    {
        speedKillsObjective.currentValue = 0;
    }

    public void SetKillsToZero()
    {
        killsWithoutDmgObjective.currentValue = 0;
    }

    void RandomBuff()
    {
        probList[0].cumulativeProb = probList[0].probability;
        int randomNumber = Random.Range(0, 100);

        if (probList[0].cumulativeProb > randomNumber)
        {
            buffID = probList[0].buffID;
            buffValue = probList[0].buffValue;
            return;
        }

        for (int i = 1; i < probList.Count; ++i)
        {
            probList[i].cumulativeProb = probList[i].probability + probList[i - 1].cumulativeProb;

            if (probList[i].cumulativeProb > randomNumber)
            {
                buffID = probList[i].buffID;
                buffValue = probList[i].buffValue;
                return;
            }
        }
    }

    void ShowUI()
    {
        if (GetComponent<SetupLocalPlayer>().CheckIfLocalPlayer())
        {
            passedInCanvas.transform.FindChild("Buff" + buffID.ToString()).gameObject.SetActive(true);
        }
    }

    void GetInput()
    {
        if (GetComponent<SetupLocalPlayer>().CheckIfLocalPlayer())
        {
            //buff self
            if (Input.GetKeyDown(KeyCode.Q))
            {
                GetComponent<Player_NetworkManager>().CmdUpdateBuffs(GetComponent<Player_NetworkManager>().netId, buffID, buffValue);
                HideUI();
                buffSelected = true;
            }
            //debuff enemy
            else if (Input.GetKeyDown(KeyCode.E))
            {
                GetComponent<Player_NetworkManager>().CmdUpdateBuffs(GetComponent<Player_Health>().enemy.GetComponent<Player_NetworkManager>().netId, buffID, -buffValue);
                HideUI();
                buffSelected = true;
            }
        }
    }

    void HideUI()
    {
        if (GetComponent<SetupLocalPlayer>().CheckIfLocalPlayer())
        {
            passedInCanvas.transform.FindChild("Buff" + buffID.ToString()).gameObject.SetActive(false);
        }
    }

    void ObjectiveCompleted()
    {

    }
}
