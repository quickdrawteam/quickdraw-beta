﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Player_NetworkManager : NetworkBehaviour
{
    public GameObject tracerObj = null;
    public GameObject bulletHoleObject = null;
    public GameObject sparks = null;
    //called from client to tell the server that a player has been damaged
    [Command]
    public void CmdDamagePlayer(NetworkInstanceId ID, float damage)
    {
        //then relay that message to all clients telling them that the object has taken damage
        GameObject target = NetworkServer.FindLocalObject(ID);
        target.GetComponent<Player_NetworkManager>().RpcTakeDamage(damage);
    }

    //sends the updated health to all the clients
    [ClientRpc]
    void RpcTakeDamage(float damage)
    {
        GetComponent<Player_Health>().TakeDamage(damage);
    }

    // Player buff server update
    [Command]
    public void CmdUpdateBuffs(NetworkInstanceId ID, int buffID, float value)
    {
        GameObject target = NetworkServer.FindLocalObject(ID);
        target.GetComponent<Player_NetworkManager>().RpcUpdateBuffs(target, buffID, value);
    }

    [ClientRpc]
    public void RpcUpdateBuffs(GameObject target, int buffID, float value)
    {
        GetComponent<Player_Buffs>().ChooseBuff(target, buffID, value);
    }

    [Command]
    public void CmdChangeScene(string levelName)
    {
        FindObjectOfType<NetworkManager>().ServerChangeScene(levelName);
    }

    [Command]
    public void CmdServerSpawnObject(GameObject objToSpawn, Vector3 position, Quaternion rotation)
    {
        GameObject go = Instantiate(objToSpawn, position, rotation) as GameObject;
        NetworkServer.Spawn(objToSpawn);
    }

    [Command]
    public void CmdServerSpawnTracer(NetworkInstanceId ID, Vector3 position, Vector3 direction, Quaternion rotation, float force)
    {
        GameObject target = NetworkServer.FindLocalObject(ID);
        target.GetComponent<Player_NetworkManager>().RpcServerSpawnTracer(target, position, direction, rotation, force);
    }

    [ClientRpc]
    public void RpcServerSpawnTracer(GameObject target, Vector3 position, Vector3 direction, Quaternion rotation, float force)
    {
        target.GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().ShootTracer(position, direction, rotation, force);
    }

    [Command]
    public void CmdServerSpawnWallHitEffects(NetworkInstanceId ID, Quaternion hitRotation, Vector3 position)
    {
        GameObject target = NetworkServer.FindLocalObject(ID);
        RpcServerSpawnWallHitEffects(target, hitRotation, position);
    }

    [ClientRpc]
    public void RpcServerSpawnWallHitEffects(GameObject target, Quaternion hitRotation, Vector3 position)
    {
        target.GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().CreateBulletHole(hitRotation, position);
    }

    [Command]
    public void CmdServerSpawnSparks(Quaternion rotation, Vector3 position)
    {
        GameObject spark = Instantiate(sparks, position, rotation) as GameObject;
        spark.GetComponent<GIF_Script>().destroyOnEnd = false;
        spark.transform.Rotate(new Vector3(0, 0, Random.Range(0, 360)));
        spark.transform.position += spark.transform.forward * -0.001f;

        NetworkServer.Spawn(spark);
    }

    [Command]
    public void CmdTeleportPlayers(NetworkInstanceId player, NetworkInstanceId player2, Vector3 pos, Vector3 pos2, Vector3 posToLookAt)
    {
        GameObject target = NetworkServer.FindLocalObject(player);
        GameObject target2 = NetworkServer.FindLocalObject(player2);
        target.GetComponent<Player_NetworkManager>().RpcTeleportPlayer(target, pos, posToLookAt);
        target2.GetComponent<Player_NetworkManager>().RpcTeleportPlayer(target2, pos2, posToLookAt);
    }

    [ClientRpc]
    public void RpcTeleportPlayer(GameObject target, Vector3 position, Vector3 posToLookAt)
    {
        target.transform.position = position;
        target.transform.LookAt(posToLookAt);
    }



    [Command]
    public void CmdWinState(NetworkInstanceId player)
    {
        GameObject target = NetworkServer.FindLocalObject(player);
        RpcWinState(target);

    }

    [ClientRpc]
    public void RpcWinState(GameObject target)
    {
        target.GetComponent<Player_Health>().winner = true;
    }


    [Command]
    public void CmdSyncAnimation(NetworkInstanceId player, string animName, bool play)
    {
        GameObject target = NetworkServer.FindLocalObject(player);
        RpcSyncAnimation(target, animName, play);
        
    }


    /// <summary>
    /// Animation names are:
    ///Run,
    ///Walk,
    ///Reload,
    ///StrafeRight,
    ///StrafeLeft,
    ///SwitchWeapon
    /// </summary>
    [ClientRpc]
    public void RpcSyncAnimation(GameObject target, string animName, bool play)
    {
        if(animName == "Run")
            target.GetComponent<Player_SyncAnims>().isRunning = play;
        else if(animName == "Walk")
            target.GetComponent<Player_SyncAnims>().isWalking = play;
        else if (animName == "Reload")
            target.GetComponent<Player_SyncAnims>().isReloading = play;
        else if (animName == "StrafeRight")
            target.GetComponent<Player_SyncAnims>().isStrafingRight = play;
        else if (animName == "StrafeLeft")
            target.GetComponent<Player_SyncAnims>().isStrafingLeft = play;
        else if (animName == "SwitchWeapon")
            target.GetComponent<Player_SyncAnims>().SwitchWeapon = play;
    }

}
