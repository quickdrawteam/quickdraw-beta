﻿using UnityEngine;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;
using UnityEngine.UI;

public class Player_Buffs : MonoBehaviour
{
    [SerializeField]
    public List<Sprite> buffSprites;

    public float imageOffset            = 1.0f;
    public float imgScale               = 0.5f;
    public float shrinkFactor           = 0.01f;
    public float initScaleFactor        = 2;

    private float imgScaledUp           = 0;

    public GameObject firstImgEndPos    = null;
    public GameObject initImgPos        = null;

    private Vector3 currentImgPos       = Vector3.zero;
    private Vector3 prevImgPos          = Vector3.zero;
    private Vector3 initImgScale        = Vector3.zero;
    private float initImgOffset         = 0;
    private Image newImg                = null;
    private GameObject newObj           = null;
    private Player_NetworkManager netManager;
    private Player_Health healthScript;

    public float speed                  = 10;
    public bool explosiveHeadshot       = false;
    public float headshotActiveTime = 10;

    private float headshotTimer;
    private float startTime;
    private float journeyLength;
    private bool distanceChecked;


    public enum SelectedBuff
    {
        NONE = 0,           // Buff id of 0
        MOVEMENT,           // Buff id of 1
        VISION,             // Buff id of 2
        DAMAGE,             // Buff id of 3
        HEALTH,             // Buff id of 4
        SPAWN_RATE,         // Buff id of 5
        INVERT_CONTROLS,    // Buff id of 6
        EXPLOSIVE_BULLETS,  // Buff id of 7
        REDUCE_AMMO,        // Buff id of 8
        EXPLOSIVE_HEADSHOTS // Buff id of 9

    }


    // Use this for initialization
    void Start()
    {
        imgScaledUp = imgScale * initScaleFactor;
        startTime = Time.time;

        netManager = GetComponent<Player_NetworkManager>();
        healthScript = GetComponent<Player_Health>();
    }

    // Update is called once per frame
    void Update()
    {
        if (netManager.isLocalPlayer)
        {

            // Updates previous image position 
            if (firstImgEndPos != null && prevImgPos == Vector3.zero)
                prevImgPos = firstImgEndPos.transform.position;
            else if (firstImgEndPos == null && healthScript.canvasInstance != null)
                firstImgEndPos = healthScript.canvasInstance.transform.FindChild("BuffHolder").transform.GetChild(0).gameObject;


            if (initImgPos == null && healthScript.canvasInstance != null)
                initImgPos = healthScript.canvasInstance.transform.FindChild("BuffInitPos").gameObject;
            else if (newObj != null && initImgPos != null)
            {
                // Lerp the image towards the set target
                if (Vector3.Distance(newObj.transform.position, currentImgPos) > 0)
                {
                    if (!distanceChecked)
                    {
                        journeyLength = Vector3.Distance(initImgPos.transform.position, currentImgPos);
                        distanceChecked = true;
                        startTime = Time.time;
                    }
                    float distCovered = (Time.time - startTime) * speed * 300;
                    float fracJourney = distCovered / journeyLength;
                    newObj.transform.position = Vector3.Lerp(initImgPos.transform.position, currentImgPos, fracJourney);
                }

                // Scale the image down as it Lerps
                if(newObj.transform.localScale.magnitude > (initImgScale * imgScale).magnitude)
                {
                    newObj.transform.localScale *= 1 - shrinkFactor * 2;
                    Mathf.Clamp(newObj.transform.localScale.magnitude, (initImgScale * imgScale).magnitude, int.MaxValue);
                }
            }

            if (explosiveHeadshot)
                headshotTimer -= Time.deltaTime;
            
            if (headshotTimer <= 0)
                explosiveHeadshot = false;
        }
    }

    void BuffPlayer(GameObject player, SelectedBuff buff, float value)
    {
        if (netManager.isLocalPlayer)
        {
            // Used to determine whether the buff in question is a buff or debuff
            bool Buffing = false;

            if (value > 0)
                Buffing = true;

            switch (buff)
            {
                case SelectedBuff.NONE:
                    break;
                case SelectedBuff.MOVEMENT:
                    {

                        player.GetComponent<Player_Movement>().initMoveSpeed += (int)value;
                        if (Buffing)
                        {
                            GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().activeMaterial =
                                GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().speedMaterial;

                            transform.GetChild(0).transform.GetChild(0).transform.GetChild(2).transform.GetChild(0).transform.GetChild(0).GetComponent<PlayerShaderManager>().activeMaterial =
                               transform.GetChild(0).transform.GetChild(0).transform.GetChild(2).transform.GetChild(0).transform.GetChild(0).GetComponent<PlayerShaderManager>().speedMaterial;
                            AddImage(buffSprites[0]);
                        }
                        else
                            AddImage(buffSprites[1]);
                        break;
                    }
                case SelectedBuff.VISION:
                    {
                        player.transform.FindChild("Camera").GetComponent<VignetteAndChromaticAberration>().intensity *= value;
                        if (Buffing)
                            AddImage(buffSprites[2]);
                        else
                            AddImage(buffSprites[3]);
                        break;
                    }
                case SelectedBuff.DAMAGE:
                    {
                        player.GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().bulletDamage += (int)value;
                        if (Buffing)
                            AddImage(buffSprites[4]);
                        else
                            AddImage(buffSprites[5]);
                        break;
                    }
                case SelectedBuff.HEALTH:
                    {

                        player.GetComponent<Player_Health>().currentHealth += (int)value;
                        player.GetComponent<Player_Health>().maxHealth += (int)value;


                        if (Buffing)
                        {
                            GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().activeMaterial = 
                                GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().healthMaterial;

                            transform.GetChild(0).transform.GetChild(0).transform.GetChild(2).transform.GetChild(0).transform.GetChild(0).GetComponent<PlayerShaderManager>().activeMaterial =
                                transform.GetChild(0).transform.GetChild(0).transform.GetChild(2).transform.GetChild(0).transform.GetChild(0).GetComponent<PlayerShaderManager>().healthMaterial;

                            AddImage(buffSprites[6]);
                        }
                        else
                        {
                            AddImage(buffSprites[7]);
                        }
                    }
                    break;
                case SelectedBuff.SPAWN_RATE:

                    break;
                case SelectedBuff.INVERT_CONTROLS:

                    break;
                case SelectedBuff.EXPLOSIVE_BULLETS:

                    break;
                case SelectedBuff.REDUCE_AMMO:

                    break;
                case SelectedBuff.EXPLOSIVE_HEADSHOTS:
                    {
                        explosiveHeadshot = true;
                        headshotTimer = headshotActiveTime;

                        Debug.Log("Explosive Headshots Active!");

                        //if (Buffing)
                        //    AddImage(buffSprites[6]);
                        //else
                        //    AddImage(buffSprites[7]);

                        break;
                    }
                default:
                    break;
            }
        }
    }

    void BuffPlayer(GameObject player, SelectedBuff buff, bool activate)
    {

    }

    public void ChooseBuff(GameObject player, int buffID, float value)
    {
        SelectedBuff buff = SelectedBuff.NONE;

        switch (buffID)
        {
            case 0:
                {
                    buff = SelectedBuff.NONE;
                    break;
                }
            case 1:
                {
                    buff = SelectedBuff.MOVEMENT;
                    break;
                }
            case 2:
                {
                    buff = SelectedBuff.VISION;
                    break;
                }
            case 3:
                {
                    buff = SelectedBuff.DAMAGE;
                    break;
                }
            case 4:
                {
                    buff = SelectedBuff.HEALTH;
                    break;
                }
            case 5:
                {
                    buff = SelectedBuff.SPAWN_RATE;
                    break;
                }
            case 6:
                {
                    buff = SelectedBuff.INVERT_CONTROLS;
                    break;
                }
            case 7:
                {
                    buff = SelectedBuff.EXPLOSIVE_BULLETS;
                    break;
                }
            case 8:
                {
                    buff = SelectedBuff.REDUCE_AMMO;
                    break;
                }
            case 9:
                {
                    buff = SelectedBuff.EXPLOSIVE_HEADSHOTS;
                    break;
                }
            default:
                buff = SelectedBuff.NONE;
                break;
        }

        BuffPlayer(player, buff, value);
    }
    
    void AddImage(Sprite sprite)
    {
        // Set Position for Image
        currentImgPos = prevImgPos;
        currentImgPos.x += initImgOffset;

        // Create Image Object
        newObj = new GameObject();
        newImg = newObj.AddComponent<Image>();
        newImg.sprite = sprite;

        // Set Image's parent and position
        newObj.transform.position = initImgPos.transform.position;
        newObj.GetComponent<RectTransform>().SetParent(GetComponent<Player_Health>().canvasInstance.transform);

        // Alter Image's scale
        initImgScale = newObj.transform.localScale;
        newObj.transform.localScale *= imgScaledUp;
        newObj.SetActive(true);
        
        // Edit values for next Image
        prevImgPos = currentImgPos;
        initImgOffset = imageOffset;
    }
}
