﻿using UnityEngine;
using System.Collections;

public class Camera_Movement : MonoBehaviour
{
    public float XSensitivity = 2f;
    public float YSensitivity = 2f;
    public bool clampVerticalRotation = true;

    private GameObject player;

    public float MinimumX = -90F;
    public float MaximumX = 90F;


    private bool menuOpen = false;
    void Start()
    {
        player = transform.root.gameObject;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = true;
    }

    void Update()
    {
        if (transform.root.GetComponent<SetupLocalPlayer>().CheckIfLocalPlayer())
        {
            float yRot = Input.GetAxisRaw("Mouse X") * XSensitivity;
            float xRot = Input.GetAxisRaw("Mouse Y") * YSensitivity;

            player.transform.localRotation *= Quaternion.Euler(0f, yRot, 0f);
            transform.localRotation *= Quaternion.Euler(-xRot, 0f, 0f);

            if (clampVerticalRotation)
                transform.localRotation = ClampRotationAroundXAxis(transform.localRotation);

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                EscapePressed();

                if (!menuOpen)
                {
                    Cursor.lockState = CursorLockMode.Locked;
                    Cursor.visible = false;
                }
            }


        }


    }

    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, MinimumX, MaximumX);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }

    void EscapePressed()
    {
        menuOpen = !menuOpen;

        if (menuOpen)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else if (!menuOpen)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }
}
