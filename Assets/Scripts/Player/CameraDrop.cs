﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CameraDrop : MonoBehaviour
{
    public bool drop = false;
    public string victoryText;
    public string defeatText;
    private bool dropped = false;
    private Rigidbody rb;
    private Transform trans;
    private bool positionCorrected = false;
    private Canvas playerCanvas = null;


    // Use this for initialization
    void Start ()
    {
        //rb = GetComponent<Rigidbody>();
        //rb.useGravity = false;
        GetComponent<SphereCollider>().enabled = false;
        GetComponent<SphereCollider>().isTrigger = true;

        GetComponent<BoxCollider>().enabled = false;
        GetComponent<BoxCollider>().isTrigger = true;
        trans = transform;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(!positionCorrected && transform.position != trans.position && dropped)
        {
            transform.position = trans.position;
            transform.rotation = trans.rotation;

            if (transform.position == trans.position)
            {
                transform.position = new Vector3(transform.position.x, transform.position.y + 3, transform.position.z);
                positionCorrected = true;
            }
        }

	    if(drop && !dropped)
        {
            rb = gameObject.AddComponent<Rigidbody>();
            //rb.useGravity = true;
            GetComponent<SphereCollider>().enabled = true;
            GetComponent<SphereCollider>().isTrigger = false;
            GetComponent<BoxCollider>().enabled = true;
            GetComponent<BoxCollider>().isTrigger = false;
            GetComponent<Camera_Movement>().enabled = false;
            GameObject enemyPlayer = transform.root.GetComponent<Player_Health>().enemy;

            if(enemyPlayer != null)
                transform.root.GetComponent<Player_NetworkManager>().CmdWinState(enemyPlayer.GetComponent<Player_NetworkManager>().netId);
            transform.root.GetChild(0).GetComponent<CapsuleCollider>().enabled = false;
            transform.parent.FindChild("Weapons").gameObject.SetActive(false);
            transform.parent.FindChild("Head").gameObject.SetActive(false);

            trans = transform.root.transform;
            playerCanvas = transform.root.GetComponent<Player_Health>().canvasInstance;
            DisableUI();
            transform.parent = null;



            GetComponent<Animator>().enabled = true;

            dropped = true;
        }
	}

    public void FadeUI()
    {
        playerCanvas.transform.FindChild("DeathFade").gameObject.SetActive(true);

        GameObject.Find("LobbyManager").transform.FindChild("BackToMenu").gameObject.SetActive(true);

        if(!dropped)
            playerCanvas.transform.FindChild("DeathFade").transform.GetChild(0).GetComponent<Text>().text = victoryText;
        else
            playerCanvas.transform.FindChild("DeathFade").transform.GetChild(0).GetComponent<Text>().text = defeatText;


    }

    public void DisableUI()
    {
        playerCanvas = transform.root.GetComponent<Player_Health>().canvasInstance;
        for (int i = 0; i < playerCanvas.transform.childCount; ++i)
        {
            if (playerCanvas.transform.GetChild(i) != playerCanvas.transform.FindChild("DeathFade"))
                playerCanvas.transform.GetChild(i).gameObject.SetActive(false);
        }
    }

    public void TurnOffScripts()
    {
        GetComponent<Camera_Movement>().enabled = false;
        transform.parent.FindChild("Weapons").gameObject.SetActive(false);
        transform.parent.FindChild("Head").gameObject.SetActive(false);
    }
}
