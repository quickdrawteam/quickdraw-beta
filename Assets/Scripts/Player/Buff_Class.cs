﻿using UnityEngine;
using System.Collections.Generic;

public class Buff_Class : MonoBehaviour
{
    public int buffID;
    public float buffValue;
    public float buffProbability;

    #region Buffs
    public List<BuffProb> probList;
    public float movementBuffVal;
    public float healthBuffVal;
    public float damageBuffVal;
    public float speedProbability;
    public float healthProbability;
    public float damageProbability;

    public class BuffProb
    {
        public float probability;
        public float cumulativeProb;
        public float buffValue;
        public int buffID;
    }

    public bool buffSelected = false;
    public bool active;
    #endregion

    void Start()
    {
        #region Buff Settings
        probList = new List<BuffProb>();    //Instantiates new list

        BuffProb speed = new BuffProb();   //Making new speed value
        BuffProb health = new BuffProb();   //Making new health value
        BuffProb damage = new BuffProb();   //Making new damage value

        // Movement
        speed.cumulativeProb = 0;
        speed.probability = speedProbability;
        speed.buffID = 1;
        speed.buffValue = movementBuffVal;

        // Health
        health.cumulativeProb = 0;
        health.probability = healthProbability;
        health.buffID = 4;
        health.buffValue = healthBuffVal;

        // Damage
        damage.cumulativeProb = 0;
        damage.probability = damageProbability;
        damage.buffID = 3;
        damage.buffValue = damageBuffVal;

        probList.Add(health);
        probList.Add(speed);
        probList.Add(damage);
        #endregion

        RandomBuff();
    }

    // Update is called once per frame
    public void Update()
    {

    }

    void RandomBuff()
    {
        probList[0].cumulativeProb = probList[0].probability;
        int randomNumber = Random.Range(0, 100);

        if (probList[0].cumulativeProb > randomNumber)
        {
            buffID = probList[0].buffID;
            buffValue = probList[0].buffValue;
            return;
        }

        for (int i = 1; i < probList.Count; ++i)
        {
            probList[i].cumulativeProb = probList[i].probability + probList[i - 1].cumulativeProb;

            if (probList[i].cumulativeProb > randomNumber)
            {
                buffID = probList[i].buffID;
                buffValue = probList[i].buffValue;
                return;
            }
        }
    }


}
