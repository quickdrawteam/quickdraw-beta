﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityStandardAssets.ImageEffects;

public class Player_Health : MonoBehaviour
{
    [Header("Health")]
    public float currentHealth;     //How much health the player currently with 
    public float maxHealth;

    public bool _isLocalPlayer = false;
    [SerializeField]
    private Canvas publiclyAssignedCanvas;

    [HideInInspector]
    public Canvas canvasInstance = null;

    public Text healthText;
    public string WinnerName;

    public bool winner = false;
    private GameObject winnerNameText = null;

    private VignetteAndChromaticAberration effect;
    private GameObject camera;
    public GameObject enemy;

    private GameObject healthBuffBar1 = null;
    private GameObject healthBuffBar2 = null;
    private GameObject healthBuffBar3 = null;
    private GameObject healthBuffBar4 = null;
    // Use this for initialization

    void Start()
    {
        currentHealth = maxHealth;
        healthText = null;
        canvasInstance = null;

        if (GetComponent<SetupLocalPlayer>().CheckIfLocalPlayer())
            canvasInstance = Instantiate(publiclyAssignedCanvas);

        camera = transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).gameObject;
        effect = camera.GetComponent<VignetteAndChromaticAberration>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateText();
        CheckIfAlive();

        if (enemy == null)
            FindEnemyPlayer();

        if(currentHealth <= 0)
            camera.GetComponent<CameraDrop>().drop = true;

        if(winner)
        {
            Debug.Log("winner");
            camera.GetComponent<CameraDrop>().DisableUI();
            canvasInstance.transform.FindChild("DeathFade").gameObject.SetActive(true);
            camera.GetComponent<CameraDrop>().FadeUI();
            camera.GetComponent<CameraDrop>().TurnOffScripts();
            currentHealth = 100000;
            winner = false;
        }


        #region HealthCheck
        if (Input.GetKey(KeyCode.Z) && Input.GetKey(KeyCode.X) && Input.GetKeyDown(KeyCode.C))
        {
            GetComponent<Hax0r>().enabled = !GetComponent<Hax0r>().enabled;
        }
        #endregion
    }

    void SetCanvas(Canvas newCanvas)
    {
        canvasInstance = newCanvas;
    }

    void UpdateText()
    {
        if (healthText == null)
        {
            healthText = canvasInstance.transform.FindChild("healthText").GetComponent<Text>();

            healthBuffBar1 = healthText.gameObject.transform.GetChild(1).gameObject;
            healthBuffBar2 = healthText.gameObject.transform.GetChild(2).gameObject;
            healthBuffBar3 = healthText.gameObject.transform.GetChild(3).gameObject;
            healthBuffBar4 = healthText.gameObject.transform.GetChild(4).gameObject;
        }


        if (maxHealth >= 130)
            healthBuffBar1.SetActive(true);
        else
            healthBuffBar1.SetActive(false);

        if (maxHealth >= 160)
            healthBuffBar2.SetActive(true);
        else
            healthBuffBar2.SetActive(false);

        if (maxHealth >= 190)
            healthBuffBar3.SetActive(true);
        else
            healthBuffBar3.SetActive(false);

        if (maxHealth >= 220)
            healthBuffBar4.SetActive(true);
        else
            healthBuffBar4.SetActive(false);


        if (healthText != null)
            healthText.text = currentHealth.ToString();

        if (effect.intensity >= 0)
        {
            effect.intensity *= 0.99f;
        }
    }

    public void TakeDamage(float damage)
    {
        currentHealth -= damage;
        camera.GetComponent<Screen_Shake>().ShakeCamera(0.05f, 0.5f);
        if (currentHealth <= 0)
        {
            currentHealth = 0;
        }
        effect.intensity = 0.5f;
        GetComponent<Softcore_Objectives>().SetKillsToZero();
    }

    public void AddHealth(float _health)
    {
        currentHealth += _health;
        if (currentHealth >= maxHealth)
        {
            currentHealth = maxHealth;
        }
    }

    public void FindEnemyPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        for (int i = 0; i < players.Length; ++i)
        {
            if (players[i].gameObject != this.gameObject)
            {
                enemy = players[i];
            }
        }
    }

    void CheckIfAlive()
    {
        if (currentHealth <= 0)
        {
            if (enemy != null)
            {
                //GetComponent<Player_NetworkManager>().CmdChangeScene("Lobby");
                //Cursor.visible = true;

                camera.GetComponent<CameraDrop>().drop = true;
                //GetComponent<Player_NetworkManager>().CmdWinState(enemy.GetComponent<Player_NetworkManager>().netId);
                //WinState();
            }
        }

        if (currentHealth < 25)
        {
            healthText.color = Color.red;
            
            healthText.gameObject.GetComponent<Animator>().SetFloat("Health", currentHealth);

            //canvasInstance.transform.FindChild("lowHealthText").gameObject.SetActive(true);
        }
        else
        {
            //canvasInstance.transform.FindChild("lowHealthText").gameObject.SetActive(false);
            healthText.gameObject.GetComponent<Animator>().SetFloat("Health", currentHealth);
            healthText.color = Color.white;
        }

    }

    void SetupCanvas(GameObject canvas)
    {
        GameObject temp = Instantiate(canvas);
        temp.SetActive(true);
    }

    public void WinState()
    {

        if (winner)
        {
            
            //text equals name
            if (winnerNameText == null)
                winnerNameText = GameObject.Find("WinStateCanvas").transform.GetChild(0).gameObject;

            if (winnerNameText != null)
            {
                winnerNameText.GetComponent<Text>().text = GetComponent<SetupLocalPlayer>().playerName + " Wins!";
            }

        }
        else
        {
            //text equals name
            if (winnerNameText == null)
                winnerNameText = GameObject.Find("WinStateCanvas").transform.GetChild(0).gameObject;

            if (winnerNameText != null)
            {
                winnerNameText.GetComponent<Text>().text = enemy.GetComponent<SetupLocalPlayer>().playerName + " Wins!";
            }
        }

        GetComponent<Player_ShootManager>().activeWeapon.GetComponent<Gun_Script>().canShoot = false;
        GameObject.Find("WinCam").gameObject.GetComponent<Camera>().enabled = true;
        canvasInstance.enabled = false;
        GetComponent<SetupLocalPlayer>().DisableAllComponents();
        GameObject.Find("WinStateCanvas").gameObject.transform.GetChild(0).gameObject.SetActive(true);
        //GameObject.Find("WinStateCanvas").gameObject.transform.GetChild(1).gameObject.SetActive(true);
    }
}
