﻿using UnityEngine;
using System.Collections;

public class PlayerShaderManager : MonoBehaviour
{
    [HideInInspector]
    public Material defaultMaterial;
    [HideInInspector]
    public Material activeMaterial = null;
    public Material healthMaterial = null;
    public Material speedMaterial = null;

    bool materialInvokeCalled = false;
    float RimPower = 0;

	// Use this for initialization
	void Start ()
    {
        defaultMaterial = GetComponent<Renderer>().material;
        activeMaterial = defaultMaterial;
    }
	
	// Update is called once per frame
	void Update ()
    {
        GetComponent<Renderer>().material = activeMaterial;
        if(activeMaterial != defaultMaterial)
        {
            Pulse();
        }
	}

    void Pulse()
    {
        RimPower = Mathf.PingPong(Time.time * 5, 5);

        GetComponent<Renderer>().material.SetFloat("_RimPower", RimPower + 1);
        if (!materialInvokeCalled)
        {
            Invoke("ResetMaterial", 3);
            materialInvokeCalled = true;
        }
    }

    void ResetMaterial()
    {
        activeMaterial = defaultMaterial;
        materialInvokeCalled = false;
    }
}
