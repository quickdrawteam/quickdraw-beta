﻿using UnityEngine;
using System.Collections.Generic;

public class Player_Buff_Manager : MonoBehaviour
{
    [HideInInspector]
    public List<int> activeBuffs;
    private GameObject player;
    private GameObject enemyPlayer;
    private Canvas canvas;
    #region 
    public float movementBuffVal = 5.0f;
    public float healthBuffVal = 30.0f;
    public float damageBuffVal = 30.0f;

    private float buffValue;
    private int currentBuffID;
    #endregion

    void Start ()
    {
        activeBuffs = new List<int>();
	}
	
    void Setup()
    {
        FindLocalPlayer();
        if (canvas == null)
            canvas = player.GetComponent<Player_Health>().canvasInstance;
    }

    void Update ()
    {
        Setup();
        UpdateZerothElement();
	}

    void UpdateZerothElement()
    {
        if (activeBuffs.Count <= 0)
            return;
        currentBuffID = activeBuffs[0];
        SetBuffValue(activeBuffs[0]);
        ShowUI();
        GetInput(activeBuffs[0]);
    }

    void SetBuffValue(int buffID)
    {
        switch (buffID)
        {
            case 0:
                buffValue = 0;
                break;
            case 1:
                buffValue = movementBuffVal;
                break;
            case 2:
               // buffValue = visionBuffVal;
                break;
            case 3:
                buffValue = damageBuffVal;
                break;
            case 4:
                buffValue = healthBuffVal;
                break;
            case 5:
                //buffValue
                break;
            case 6:
               // buffValue
                break;
            case 7:
                //buffValue
                break;
            case 8:
                //buffValue
                break;
        }
    }

    void GetInput(int a_BuffID)
    {
        //GetComponent<Player_NetworkManager>().CmdUpdateBuffs(hit.collider.GetComponent<Player_NetworkManager>().netId, 3, -50.0f);
        if (player.GetComponent<SetupLocalPlayer>().CheckIfLocalPlayer())
        {
            //buff self
            if (Input.GetKeyDown(KeyCode.Q))
            {
                player.GetComponent<Player_NetworkManager>().CmdUpdateBuffs(player.GetComponent<Player_NetworkManager>().netId, a_BuffID, buffValue);
                player.GetComponent<Player_Buffs>().initImgPos = canvas.transform.FindChild("Buff" + a_BuffID.ToString()).transform.GetChild(0).gameObject;
                HideUI();
                activeBuffs.RemoveAt(0);
            }
            //debuff enemy
            else if (Input.GetKeyDown(KeyCode.E))
            {
                player.GetComponent<Player_NetworkManager>().CmdUpdateBuffs(enemyPlayer.GetComponent<Player_NetworkManager>().netId, a_BuffID, -buffValue);
                player.GetComponent<Player_Buffs>().initImgPos = canvas.transform.FindChild("Buff" + a_BuffID.ToString()).transform.GetChild(1).gameObject;
                HideUI();
                activeBuffs.RemoveAt(0);

            }
        }
    }

    void ShowUI()
    {
        if (player.GetComponent<SetupLocalPlayer>().CheckIfLocalPlayer())
        {
            canvas.transform.FindChild("Buff" + currentBuffID.ToString()).gameObject.SetActive(true);
            player.GetComponent<Player_Buffs>().initImgPos = canvas.transform.FindChild("Buff" + currentBuffID.ToString()).gameObject;
        }
    }

    void HideUI()
    {
        if (player.GetComponent<SetupLocalPlayer>().CheckIfLocalPlayer())
        {
            canvas.transform.FindChild("Buff" + currentBuffID.ToString()).gameObject.SetActive(false);
        }
    }

    void FindLocalPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length == 2)
        {
            float player1distance = Vector3.Distance(players[0].transform.position, transform.position);
            float player2distance = Vector3.Distance(players[1].transform.position, transform.position);
            if (player1distance < player2distance)
            {
                player = players[0];
                enemyPlayer = players[1];
            }
            else
            {
                player = players[1];
                enemyPlayer = players[0];
            }
        }
        else if (players.Length == 1)
            player = players[0];

    }

    public void AddBuffToList(int buffToAdd)
    {
        activeBuffs.Add(buffToAdd);
    }

}
