﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player_Interact : MonoBehaviour
{
    public float interactDelay = 0.5f;          // How long the player will need to hold down the interact button
    public float interactRange;
    public KeyCode interactKey = KeyCode.F;

    public Image Outer = null;
    public Image Inner = null;

    private float interactTimer;

    private Canvas passedInCanvas;

    private Camera playerCam;

    public bool hasFlag;
    // Use this for initialization
    void Start()
    {
        interactTimer = interactDelay;
        passedInCanvas = GetComponent<Player_Health>().canvasInstance;
        playerCam = GetComponentInChildren<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<SetupLocalPlayer>().CheckIfLocalPlayer())
        {
            if (Outer == null)
                Outer = GetComponent<Player_Health>().canvasInstance.transform.Find("outerReticle").GetComponent<Image>();
            if (Inner == null)
                Inner = GetComponent<Player_Health>().canvasInstance.transform.Find("ReticleMain").GetComponent<Image>();


            //Interact
            if (Input.GetKey(interactKey))
                interactTimer -= Time.deltaTime;
            else
                interactTimer = interactDelay;

            if (interactTimer <= 0)
            {
                Interact();
            }


            //raycast forward every frame
            RaycastHit hit;
            if (Physics.Raycast(playerCam.transform.position, playerCam.transform.forward, out hit))
            {
                if (hit.collider.gameObject.tag == "Enemy" || hit.collider.gameObject.tag == "Head" || hit.collider.gameObject.tag == "Body")
                {
                    Outer.color = Color.red;
                    Inner.color = Color.red;
                }
                else
                {
                    Outer.color = Color.white;
                    Inner.color = Color.white;
                }


                if (hit.collider.tag == "Interactable" && hit.collider.gameObject.GetComponent<Base_Interact>().interacted == false)
                {
                    if (Vector3.Distance(transform.position, hit.transform.position) < 5)
                    {
                        passedInCanvas.transform.FindChild("InteractText").GetComponent<Text>().text = "Hold 'F' to Interact";
                    }
                }
                else
                    passedInCanvas.transform.FindChild("InteractText").GetComponent<Text>().text = "";
            }
        }
    }

    void Interact()
    {
        RaycastHit[] hits = Physics.SphereCastAll(playerCam.transform.position, 1, playerCam.transform.forward, 5, Physics.DefaultRaycastLayers);
        if (hits.Length > 0)
        {
            // Finds first interactable object and calls custom interact function
            for (int i = 0; i < hits.Length; ++i)
            {
                if (hits[i].collider.gameObject.tag == "Interactable")
                {
                    //call custom interact function
                    hits[i].collider.gameObject.GetComponent<Base_Interact>().Interact();

                    interactTimer = interactDelay;
                    return;
                }
                else
                {
                    // display error message
                    Debug.Log("Cannot Interact with " + hits[i].collider.gameObject.tag);
                    interactTimer = interactDelay;
                }
            }
        }
        else
        {
            passedInCanvas.transform.FindChild("InteractText").GetComponent<Text>().text = "";
        }
    }


    //Camera FindActiveCamera()
    //{
    //    Camera[] cameras = FindObjectsOfType<Camera>();
    //    for (int i = 0; i < cameras.Length; ++i)
    //    {
    //        if (cameras[i].enabled == true && cameras[i].tag == "PlayerCamera")
    //            return cameras[i];
    //    }
    //    return null;
    //}

}

