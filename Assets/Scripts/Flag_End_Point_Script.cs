﻿using UnityEngine;
using System.Collections;

public class Flag_End_Point_Script : MonoBehaviour
{
	void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Body")
        {
            other.transform.root.GetComponent<Player_Flag>().FlagReturned();
        }
    }
}
