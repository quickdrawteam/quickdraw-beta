﻿using UnityEngine;
using System.Collections;

public class Teleporter : MonoBehaviour
{
    public float timer;
    public float teleportTimer;
    public GameObject otherTeleporter;
    private GameObject player;
    private bool playerInside;

	// Use this for initialization
	void Start ()
    {
        otherTeleporter = transform.GetChild(0).gameObject;
        playerInside = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        FindLocalPlayer();
    }

    void FindLocalPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length == 2)
        {
            float player1distance = Vector3.Distance(players[0].transform.position, transform.position);
            float player2distance = Vector3.Distance(players[1].transform.position, transform.position);
            if (player1distance < player2distance)
            {
                player = players[0];
            }
            else
            {
                player = players[1];
            }
        }
        else if (players.Length == 1)
            player = players[0];
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && !playerInside)
        {
            playerInside = true;
            //Player is in teleporter
            //other.GetComponent<Player_Movement>().active = false;
            transform.parent.GetComponent<TeleportManager>().activeTeleports += 1;
            player = other.gameObject;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            playerInside = false;
            transform.parent.GetComponent<TeleportManager>().activeTeleports -= 1;
        }
    }

    public void SendPlayer()
    {
        if (player.GetComponent<SetupLocalPlayer>().isLocalPlayer)
        {
            player.transform.position = otherTeleporter.transform.position;
        }
    }
}
