﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class IndicatorScript : MonoBehaviour
{
    public GameObject indicatorPrefab = null;

    private Canvas playerCanvas     = null;
    private Camera playerCam        = null;
    private GameObject player       = null;
    private GameObject indicator    = null;
    private float timer;

    // Use this for initialization
    void Start ()
    {
        timer = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (timer <= 3)
        {
            playerCam = FindActiveCamera();
            FindLocalPlayer();
            timer += Time.deltaTime;
        }


        if (playerCanvas == null && player != null)
            playerCanvas = player.GetComponent<Player_Health>().canvasInstance;
        if (player == null)
            return;

        if (player.GetComponent<SetupLocalPlayer>().isLocalPlayer)
        {
            if (player != null)
            {
                if (indicator == null)
                {
                    CreateIndicator();
                }
                else
                {
                    UpdateIndicator();
                }
            }
        }
	}

    void CreateIndicator()
    {
        indicator = Instantiate(indicatorPrefab, playerCanvas.transform) as GameObject;
        indicator.transform.parent = playerCanvas.transform.GetChild(0).transform;
        indicator.SetActive(false);
    }

    void UpdateIndicator()
    {
        Vector3 screenPos = playerCam.WorldToScreenPoint(transform.position);


        // if on screen
        if (screenPos.z > 0 && screenPos.x > 0 && screenPos.x < Screen.width 
            && screenPos.y > 0 && screenPos.y < Screen.height)
        {
            indicator.SetActive(true);
            indicator.GetComponent<Image>().color = new Color(indicator.GetComponent<Image>().color.r, indicator.GetComponent<Image>().color.b,
                indicator.GetComponent<Image>().color.g, (255 - (Vector3.Distance(player.transform.position, transform.position) * 3)) / 255);
            indicator.transform.position = screenPos;
           
        }
        else
        {
            

            indicator.SetActive(true);
            
            if (screenPos.z < 0)
                screenPos *= -1;

            Vector3 screenCenter = new Vector3(Screen.width, Screen.height, 0) / 2;
            
            screenPos -= screenCenter;
            
            float angle = Mathf.Atan2(screenPos.y, screenPos.x);
            angle -= 90 * Mathf.Deg2Rad;
            
            float cos = Mathf.Cos(angle);
            float sin = -Mathf.Sin(angle);
            
            screenPos = screenCenter + new Vector3(sin * 150, cos * 150, 0);
            
            float m = cos / sin;
            
            Vector3 screenBounds = screenCenter * 0.9f;
            
            // up and down
            if(cos > 0)
                screenPos = new Vector3(screenBounds.y / m, screenBounds.y, 0);
            else
                screenPos = new Vector3(-screenBounds.y / m, -screenBounds.y, 0);
            
            // left and right
            if (screenPos.x > screenBounds.x)
                screenPos = new Vector3(screenBounds.x, screenBounds.x * m, 0);
            else if (screenPos.x < -screenBounds.x)
                screenPos = new Vector3(-screenBounds.x, -screenBounds.x * m, 0);

            //screenPos += screenCenter;

            indicator.transform.localPosition = screenPos;
            indicator.GetComponent<Image>().color = new Color(indicator.GetComponent<Image>().color.r,
            indicator.GetComponent<Image>().color.g,
            indicator.GetComponent<Image>().color.b,
            (255 - (Vector3.Distance(player.transform.position, transform.position) * 3)) / 255);
        }
    }

    void FindLocalPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        if (players.Length == 2)
        {
            float player1distance = Vector3.Distance(players[0].transform.position, transform.position);
            float player2distance = Vector3.Distance(players[1].transform.position, transform.position);
            if (player1distance < player2distance)
            {
                player = players[0];
            }
            else
            {
                player = players[1];
            }
        }
        else if (players.Length == 1)
            player = players[0];

    }

    public Camera FindActiveCamera()
    {
        Camera[] cameras = FindObjectsOfType<Camera>();
        for (int i = 0; i < cameras.Length; ++i)
        {
            if (cameras[i].enabled == true && cameras[i].tag == "PlayerCamera")
                return cameras[i];
        }
        return null;
    }

    public void DestroyIndicator(float delay)
    {
        if(indicator != null)
            Destroy(indicator, delay);
    }
}
