﻿using UnityEngine;
using System.Collections;

public class TeleportManager : MonoBehaviour
{

    private GameObject teleporter1;
    private GameObject teleporter2;

    [HideInInspector]
    public int activeTeleports;



    // Use this for initialization
    void Start ()
    {
        teleporter1 = transform.GetChild(0).gameObject;
        teleporter2 = transform.GetChild(1).gameObject;
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (activeTeleports == 2 && teleporter1 != null && teleporter2 != null)
        {
            teleporter1.GetComponent<Teleporter>().SendPlayer();
            teleporter2.GetComponent<Teleporter>().SendPlayer();
            gameObject.GetComponent<TeleportManager>().enabled = false;
        }
        else if (teleporter1 != null && teleporter2 != null)
        {
            Debug.Log("Teleporter Not set");
        }
	}
}
