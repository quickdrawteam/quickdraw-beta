﻿using UnityEngine;
using System.Collections;

public class Audio_Script : MonoBehaviour
{
    public AudioSource audio;
    public float delay;
    private float timer;
    private bool played;

    // Use this for initialization
    void Start()
    {
        timer = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (!played)
        {
            timer += Time.deltaTime;
            if (timer > delay)
                if (!audio.isPlaying)
                {
                    audio.Play();
                    played = true;
                }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            audio.Play();
        }
    }
}
