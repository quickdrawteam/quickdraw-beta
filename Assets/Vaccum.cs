﻿using UnityEngine;
using System.Collections;

public class Vaccum : MonoBehaviour 
{
    public float vaccumSpeed;

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Body" || other.gameObject.tag == "Player")
        {
            if (other.gameObject.transform.root.GetComponent<Player_Health>().currentHealth > 0)
            {
                Vector3 disFromMeToPlayer = transform.position - other.gameObject.transform.position;
                transform.parent.transform.position -= disFromMeToPlayer.normalized * Time.deltaTime * vaccumSpeed;
            }
        }
    }
}
